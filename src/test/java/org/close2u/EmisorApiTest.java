package org.close2u;

import io.swagger.apiemisorjavaclient.model.ComprobanteFlt;
import org.close2u.EmisorApi.Controller.EmisorApiController;


import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.junit.runner.JUnitCore;
import  util.*;
import static io.restassured.RestAssured.given;

/**
 * Created by developer-1 on 01/12/2017.
 */
public class EmisorApiTest extends EmisorApiController{

        EmisorApiController dao = new EmisorApiController();

    public static void main(String args[]) {
       // JUnitCore.runClasses(EmisorApiTest.class);
    }


    @Test
    public void registrarFacturaIGV() throws IOException {
        dao.registrarFactura(Data.FACTURA_GRAVADO_IGV);

    }

    @Test
    public void registrarFactura() throws IOException {
       dao.registrarFactura(Data.FACTURA_GRAVADO);

    }


    @Test
    public void registrarBoleta() throws IOException {
         dao.registrarBoleta(Data.BOLETA_GRAVADO);
    }


    //TODO NOTA DE CREDITO BOLETA
    @Test
    public void registrarNotaCreditoBoleta()  throws IOException {
         dao.registrarNotaCredito(Data.BOLETA,Data.NOTA_CREDITO_GRAVADO);
       }

    @Test
    public void registrarNotaCreditoFactura()  throws IOException {
        dao.registrarNotaCredito(Data.FACTURA,Data.NOTA_CREDITO_GRAVADO);
    }

    //TODO NOTA DE CREDITO FACTURA
    @Test
    public void registrarNotaDebitoBoleta()  throws IOException {
        dao.registrarNotaDebito(Data.BOLETA,Data.NOTA_DEBITO_GRAVADO);
    }

    @Test
    public void registrarNotaDebitoFactura()  throws IOException {
           dao.registrarNotaDebito(Data.FACTURA,Data.NOTA_DEBITO_GRAVADO);
    }

    //TODO COMUNICACION DE BAJA
    @Test
    public void registrarResumenBajaBoleta()  throws IOException {
        dao.registrarResumenBaja(Data.BOLETA,Data.RESUMEN_BAJA);
    }


    @Test
    public void registrarResumenBajaFactura()  throws IOException {
      dao.registrarResumenBaja(Data.FACTURA,Data.RESUMEN_BAJA);
    }

    //TODO RESUMEN DIARIO
    @Test
    public void registrarResumenDiarioBoleta()  throws IOException {
         dao.registrarResumenDiario(Data.BOLETA,Data.RESUMEN_DIARIO_FILE);

    }

    /*
    @Test
    public void registrarResumenDiarioFactura()  throws IOException {
       dao.registrarResumenDiario(Data.FACTURA,Data.FACTURA_GRAVADO);
    }


    @Test
    public void registrarResumenDiarioNC()  throws IOException {
    }


    @Test
    public void registrarResumenDiarioND()  throws IOException {
    }
*/

    @Test
    public void previewBoleta() throws IOException {
          dao.previewBoleta(Data.BOLETA_GRAVADO);
    }

    @Test
    public void previewFactura() throws IOException {
         dao.previewFactura(Data.FACTURA_GRAVADO);
    }


    //TODO PREVIEW NOTA CREDITO
    @Test
    public void previewNotaCreditoFactura() throws IOException {
          dao.previewNotaCredito(Data.BOLETA,Data.NOTA_CREDITO_GRAVADO);
    }

    @Test
    public void previewNotaCreditoBoleta() throws IOException {
        dao.previewNotaCredito(Data.FACTURA,Data.NOTA_CREDITO_GRAVADO);
    }


    //TODO PREVIEW NOTA DEBITO
    @Test
    public void previewNotaDebitoFactura() throws IOException {
         dao.previewNotaDebito(Data.BOLETA,Data.NOTA_DEBITO_GRAVADO);
    }


    @Test
    public void previewNotaDebitoBoleta() throws IOException {
        dao.previewNotaDebito(Data.FACTURA,Data.NOTA_DEBITO_GRAVADO);
    }


    @Test
    public void consultarPDF() throws IOException {
        dao.consultarPDF(Data.JSON_PDF);
    }

    @Test
    public void consultarEstado() throws IOException {
        dao.consultarEstado(Data.JSON_COMPROBANTE_ESTADO);
    }


    @Test
    public void previewPlantillas() throws IOException {
        //  dao.previewPlantillas();
    }






}
