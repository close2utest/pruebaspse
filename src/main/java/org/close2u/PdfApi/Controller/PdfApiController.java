package org.close2u.PdfApi.Controller;

import org.close2u.App;
import org.close2u.PdfApi.Model.PdfApi;
import util.Constantes;
import util.Data;

import java.io.IOException;

import static io.restassured.RestAssured.given;

/**
 * Created by developer-1 on 02/01/2018.
 */
public class PdfApiController extends PdfApi{


    public void generarPdf(String documento,String proceso) throws Exception {

                    App.agregarArchvio();

        String json = App.readFile(Data.PATH_PDF+documento+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_PREVIEW_PLANTILLA).then().statusCode(200);
    }

}
