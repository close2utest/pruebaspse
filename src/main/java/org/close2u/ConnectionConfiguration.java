package org.close2u;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by developer-1 on 28/12/2017.
 */
public class ConnectionConfiguration {
    public static Connection getConnection() {
        Connection connection = null;

        try {

            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/invoice2udb", "udb_invoice2u", "2016invoice2u");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return connection;
    }
}
