package org.close2u.EmisorApi.Model;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.junit.BeforeClass;
import util.Constantes;

/**
 * Created by developer-1 on 20/12/2017.
 */
public class EmisorApi {

    private static final String BASE_PATH = Constantes.BASE_URL+ "apiemisor";

    @BeforeClass
    public static  void setup() {
        RestAssured.baseURI = BASE_PATH;
        RestAssured.authentication = RestAssured.preemptive()
                .basic(Constantes.USER, Constantes.PASSWORD);

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .addHeader("X-Auth-Token", Constantes.API_KEY)
                .addHeader("Accept", "application/json")
                .build();

        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }

}
