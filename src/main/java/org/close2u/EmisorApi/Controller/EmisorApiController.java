package org.close2u.EmisorApi.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.squareup.okhttp.ResponseBody;
import io.restassured.response.Response;
import io.swagger.apiemisorjavaclient.model.*;
import org.close2u.App;
import org.close2u.ConnectionConfiguration;
import org.close2u.EmisorApi.Model.EmisorApi;
import util.*;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;

import org.junit.BeforeClass;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

/**
 * Created by developer-1 on 30/11/2017.
 */


public class EmisorApiController extends EmisorApi{
     Gson gson = new Gson();
   Connection jdbctempalte = ConnectionConfiguration.getConnection();
    String jsonResumenBaja= null;

    String []  valor;
    public void registrarFactura(String proceso) throws IOException {

        String json = App.readFile(Data.EMISOR_FACTURA+proceso);


          given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_FACTURA)
                  .then()
                  .statusCode(200);


    }



    public void registrarBoleta(String proceso) throws IOException {


        String json = App.readFile(Data.EMISOR_BOLETA+proceso);
      //  System.out.println(new Gson().toJson(json));

        Boleta boleta = gson.fromJson(json, Boleta.class);

       // boleta.setDatosDocumento(new Principal().fechaEmision(Util.getFechaActual()));
      //  mapper.writeValueAsString(boleta);
       // System.out.println(new Gson().toJson(boleta));
       // String jsonBoleta=new Gson().toJson(boleta);

        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_BOLETA).then().statusCode(200);

    }

    public void registrarNotaCredito(String documento,String proceso) throws IOException {

        String json = App.readFile(Data.PATH_EMISOR+documento+Data.NOTA_CREDITO+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_NOTA_CREDITO).then().statusCode(200);


    }

    public void registrarNotaDebito(String documento,String proceso) throws IOException {

        String json = App.readFile(Data.PATH_EMISOR+documento+Data.NOTA_DEBITO+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_NOTA_DEBITO).then().statusCode(200);


    }


    public void getDataDocumento(String documento){

        ResumenBaja resumenBaja = gson.fromJson(jsonResumenBaja, ResumenBaja.class);
        try {
            if (Data.BOLETA.equals(documento))
            valor= App.getTipoDocumenrto(Data.SERIE_BOLETA).split("-");
        else
            valor= App.getTipoDocumenrto(Data.SERIE_FACTURA).split("-");
      /*valor= App.getGenerarFactura(Data.SERIE_BOLETA).split("-")[0];
        valor2 =  App.getGenerarFactura(Data.SERIE_FACTURA).split("-")[1];*/
        }catch (SQLException e){
            e. printStackTrace();
        }

        resumenBaja.getResumenBajaItemList().get(0).setSerie(valor[0]);
        resumenBaja.getResumenBajaItemList().get(0).setNumero(Long.parseLong( valor[1]));

        System.out.println(resumenBaja);
    }

    public void registrarResumenBaja(String documento,String proceso) throws IOException  {

         jsonResumenBaja = App.readFile(Data.PATH_EMISOR+documento+Data.COMUNICACION_BAJA+proceso);

        given()
                .contentType("application/json")
                .body(jsonResumenBaja)
                .when().put(Constantes.API_RESUMEN_BAJA).then().statusCode(200);


    }

    public void registrarResumenDiario(String documento,String proceso) throws IOException  {

       String  json = App.readFile(Data.PATH_EMISOR+documento+Data.RESUMEN_DIARIO+proceso);

        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_RESUMEN_DIARIO).then().statusCode(200);

    }



    // /preview/boleta

    public void previewBoleta(String proceso) throws IOException {

        String json = App.readFile(Data.EMISOR_BOLETA+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_PREVIEW_BOLETA).then().statusCode(200);


    }

    public void previewFactura(String proceso) throws IOException {

        String json = App.readFile(Data.EMISOR_FACTURA+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_PREVIEW_FACTURA).then().statusCode(200);


    }

    public void consultarPDF(String proceso) throws IOException {

        String json = App.readFile(Data.EMISOR_FACTURA+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_CONSULTAR_PDF).then().statusCode(200);


    }

    public void consultarEstado(String proceso) throws IOException {

        String json = App.readFile(Data.EMISOR_FACTURA+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_CONSULTAR_ESTADO).then().statusCode(200);


    }


    public void consultarXML(String proceso) throws IOException {

        String json = App.readFile(Data.EMISOR_FACTURA+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_CONSULTAR_XML).then().statusCode(200);


    }

    public void previewNotaCredito(String documento,String proceso) throws IOException {

        String json = App.readFile(Data.PATH_EMISOR+documento+Data.NOTA_CREDITO+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_NOTA_CREDITO).then().statusCode(200);


    }



    public void previewNotaDebito(String documento,String proceso) throws IOException {

        String json = App.readFile(Data.PATH_EMISOR+documento+Data.NOTA_DEBITO+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_NOTA_DEBITO).then().statusCode(200);

    }



    public void previewPlantillas(String proceso) throws IOException {

        String json = App.readFile(Data.PATH_EMISOR+proceso);
        given()
                .contentType("application/json")
                .body(json)
                .when().put(Constantes.API_PREVIEW_PLANTILLA).then().statusCode(200);

    }



}