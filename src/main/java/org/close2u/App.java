package org.close2u;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import org.apache.commons.codec.binary.Base64;
import org.close2u.invoice2uclient.ApiClient;
import org.close2u.invoice2uclient.ApiException;
import org.close2u.invoice2uclient.api.ComprobanteControllerApi;
import org.close2u.invoice2uclient.model.Factura;
import org.close2u.invoice2uclient.model.Respuesta;
import sun.misc.IOUtils;
import sun.nio.ch.IOUtil;
import sun.nio.cs.StandardCharsets;
import util.Data;
import util.Util;


import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * Hello world!
 *
 */


public class App 
{


    public static void main(String args[]) throws SQLException {
       //String s  =getGenerarFactura();
    }

    public static String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }

            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }



  public  static   String getTipoDocumenrto(String comprobante) throws SQLException {

    Statement s = ConnectionConfiguration.getConnection().createStatement();
    String serie="", numero="";
    ResultSet rs = s.executeQuery (
            "SELECT " +
            " a.SERIE,a.NUMERO " +
            " FROM invoice2udb.comprobante a " +
           " WHERE a.serie LIKE"+ comprobante +"%"+
            " order by a.FECHA_EMISION desc " +
            " limit 1 ");

        while (rs.next()){
            serie = rs.getString(1);
            numero= rs.getString(2);
        }
    return serie+"-"+numero;
}



  public static void agregarArchvio() {
      String archivoXml="";
      String rucEmisor="";
      String tipoPlantilla="";
      String plantilla ="";
        try {
            archivoXml= Util.encodeFileToBase64Binary("data/XMLtoPDF/HOLA-18.XML");
        }catch (IOException io){
            io.printStackTrace();
        }

    BufferedWriter bw = null;
    FileWriter fw = null;


        String docjson="FFA01_hola.json";
        try {
        String data = "{ " +
                "\"archivoXml\": " +
                "\" " +  archivoXml +" \", " +
              /*  "\"plantilla\": " +
                " \"" + plantilla +   "\", " +*/
                " \"rucEmisor\": " +
                " \"" + rucEmisor +   "\", " +
                " \"tipoPlantilla \": " +
                " \"" + tipoPlantilla +   "\" " +
                " } ";


        File file = new File(Data.FILE_XMLtoPDF+docjson);
        // Si el archivo no existe, se crea!
        if (!file.exists()) {
            file.createNewFile();
        }
        // flag true, indica adjuntar información al archivo.
        fw = new FileWriter(file.getAbsoluteFile(), true);
        bw = new BufferedWriter(fw);
        bw.write(data);
        System.out.println("información agregada!");
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        try {
            //Cierra instancias de FileWriter y BufferedWriter
            if (bw != null)
                bw.close();
            if (fw != null)
                fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}



}
