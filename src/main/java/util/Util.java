package util;

import com.google.common.io.Files;
import org.apache.commons.codec.binary.Base64;
import org.close2u.App;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by developer-1 on 02/01/2018.
 */
public class Util {


    public static String getFechaActual(){
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43

    return     dateFormat.format(date);

}

    public static String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);

        byte[] encoded = Base64.encodeBase64(Files.toByteArray(file));
        return new String(encoded);
    }


}
