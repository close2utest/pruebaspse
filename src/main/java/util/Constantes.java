package util;

/**
 * Created by armidaochoa on 5/27/16.
 */
public class Constantes {

    //  basePath = "https://c2udev.invoice2u.pe/apiemisor";
    //AMBIENTES DEL SISTEMA

    //public static final String BASE_URL = "https://www.invoice2u.pe/";
     //public static final String BASE_URL = "https://dev.invoice2u.pe/";
     public static final String BASE_URL = "https://c2udev.invoice2u.pe/";

      //public static final String BASE_URL    = "http://localhost:9040/";
    //public static final String BASE_URL = "http://192.168.1.11:8080/";//HEINER

// USERS c2udev
//--1
  /*public static final String USER = "desa@close2u.pe";
    public static final String PASSWORD = "desa";
    public static final String API_KEY = "MjA1NDk3NzY5NzQ=";//c2udev */

//--2
  /*public static String API_KEY = "MjA1NDk3NzY5NzQ=";
    public static String USER = "armida.ochoa@close2u.pe";
    public static String PASSWORD = "demo"; //c2udev*/
//--3

   public static final String USER = "cachorro@close2u.pe";
     public static final String PASSWORD = "demo";
     public static final String API_KEY = "MTA3MTM5NTE3MDE="; //c2udev

    // USERS PROD

  /*  public static String RUC = "20549776974";
    public static String User = "20549776974";
    public static String Password = "19940115";*/ //PROD



    // USERS DEV

    /*public static String apikey = "20512217452";
    public static String RUC = "20512217452";
    public static String User = "contabilidad@agualima.com";
    public static String Password = "agualima1234";*/ //DEV`



    public static String FechaEmision = "2016-11-22";
    public static String FechaVencimiento = "2016-12-30";




    // API
    //  /invoice2u/integracion/factura
    public static final String API_FACTURA= "invoice2u/integracion/factura";
    public static final String API_BOLETA= "invoice2u/integracion/boleta";

    public static final String API_NOTA_CREDITO= "invoice2u/integracion/nota-credito";
    public static final String API_NOTA_DEBITO= "invoice2u/integracion/nota-debito";

    public static final String API_RESUMEN_BAJA=  "invoice2u/integracion/resumen-baja";
    public static final String API_RESUMEN_DIARIO=  "invoice2u/integracion/resumen-diario";

    public static final String API_PREVIEW_BOLETA=  "preview/boleta";
    public static final String API_PREVIEW_FACTURA=  "preview/factura";
    public static final String API_PREVIEW_NOTA_CREDITO= "preview/nota-credito";
    public static final String API_PREVIEW_NOTA_DEBITO=  "preview/nota-debito";
    public static final String API_PREVIEW_PLANTILLA= "preview/plantillas";
    public static final String API_CONSULTAR_PDF= "invoice2u/integracion/consultarPdf";
    public static final String API_CONSULTAR_XML= "invoice2u/integracion/consultarXml";

    public static final String API_CONSULTAR_ESTADO= "invoice2u/integracion/consultarEstado";

    public static final String API_PDF= "secure/GenerarPdf";
    //******************************




}
