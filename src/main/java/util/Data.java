package util;

/**
 * Created by developer-1 on 13/12/2017.
 */
public class Data {



    //PROCESO BOLETA
    public static final String BOLETA_GRAVADO="boletaGravado.json";
    public static final String BOLETA_GRATUITA="boletaGratuita.json";
    public static final String BOLETA_EXONERADO="boletaGravado1.json";
    public static final String BOLETA_EXPORTACION="boletaExportacion.json";
    public static final String BOLETA_INAFECTA="boletaInafecta.json";

    //PROCESO FACTURA
    // SIN IGV
    public static final String FACTURA_GRAVADO="facturaGravado.json";
    public static final String FACTURA_GRATUITA="facturaGratuita.json";
    public static final String FACTURA_EXONERADO="facturaExonerado.json";
    public static final String FACTURA_EXPORTACION="facturaExportacion.json";
    public static final String FACTURA_INAFECTA="facturaInafecta.json";

    // CON IGV
    public static final String FACTURA_GRAVADO_IGV="facturaGravadoIGV.json";
  /*  public static final String FACTURA_GRATUITA="facturaGratuita.json";
    public static final String FACTURA_EXONERADO="facturaExonerado.json";
    public static final String FACTURA_EXPORTACION="facturaExportacion.json";
    public static final String FACTURA_INAFECTA="facturaInafecta.json";
*/
    //PROCESO NOTA DE CREDITO
    public static final String NOTA_CREDITO_GRAVADO="notaCreditoGravado.json";
    public static final String NOTA_CREDITO_GRATUITA="notaCreditoGratuita.json";
    public static final String NOTA_CREDITO_EXONERADO="notaCreditoExonerado.json";
    public static final String NOTA_CREDITO_EXPORTACION="notaCreditoExportacion.json";
    public static final String NOTA_CREDITO_INAFECTA="notaCreditoInafecta.json";

    //PROCESO NOTA DE DEBITO

    public static final String NOTA_DEBITO_GRAVADO="notaDebitoGravado.json";
    public static final String NOTA_DEBITO_GRATUITA="notaDebitoGratuita.json";
    public static final String NOTA_DEBITO_EXONERADO="notaDebitoExonerado.json";
    public static final String NOTA_DEBITO_EXPORTACION="notaDebitoExportacion.json";
    public static final String NOTA_DEBITO_INAFECTA="notaDebitoInafecta.json";


       //PROCESO COMUNICACION DE BAJA
    public static final String RESUMEN_BAJA="resumenBaja.json";

    //PROCESO RESUMEN DE DIARIO
    public static final String RESUMEN_DIARIO_FILE="resumenDiario.json";


    public static final String JSON_PDF =  "consultaPDF.json";

    public static final String JSON_COMPROBANTE_ESTADO =  "consultaEstado.json";
//+++++++++++++++++++++++++++++++++++
//DIRECTORIO


    //PATH  emisorApi
    public static final String PATH_EMISOR =  "data/emisorApi/";
    public static final String BOLETA =  "boleta/";
    public static final String FACTURA =  "factura/";


    public static final String PATH_BOLETA =  PATH_EMISOR+"boleta/";
    public static final String PATH_FACTURA =  PATH_EMISOR+"factura/";
    public static final String PATH_PDF =  "data/XMLtoPDF/";

    public static final String EMISOR_FACTURA = PATH_FACTURA;
    public static final String EMISOR_BOLETA = PATH_BOLETA;


  /*  public static final String NOTA_CREDITO=  EMISOR_BOLETA+"nota_credito/";
    public static final String NOTA_DEBITO=  EMISOR_FACTURA+"nota_debito/";
*/
   public static final String NOTA_CREDITO=  "nota_credito/";
    public static final String NOTA_DEBITO= "nota_debito/";



    public static final String RESUMEN_DIARIO=  "resumen_diario/";

    public static final String COMUNICACION_BAJA=  "comunicacion_baja/";

//SERIE

    public static final String  SERIE_BOLETA="BBV";
    public static final String  SERIE_FACTURA="FFA";

    public static final String  S_BOLETA="BOLETA";
    public static final String  S_FACTURA="FACTURA";
    //--------------------------------------

    //FILES

    public static final String  FILE_XMLtoPDF="data/XMLtoPDF/boleta/";



}
